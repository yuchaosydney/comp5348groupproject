﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MessageBus.Model;
using VideoStore.Common;
using VideoStore.Business.Entities;
using MessageBus.Interfaces;
using Microsoft.Practices.ServiceLocation;
using VideoStore.Business.Adapters.TransferService;
using VideoStore.Business.Components.Commands;
using System.ServiceModel;
using Bank.Business.Components.Interfaces;
using Bank.Business.Entities;
using VideoStore.Business.Components.Interfaces;

namespace VideoStore.Business.Adapters
{
    public class BankAdapter :IAdapter, IOperationOutcomeService
    {
        private const String cBankOutcomeAddress = "net.msmq://localhost/private/BankTransferResponseQueueTransaction1";
        public void Start()
        {
            ISubscriptionService lSubscriber = ServiceLocator.Current.GetInstance<ISubscriptionService>();
            lSubscriber.Subscribe(typeof(Command).AssemblyQualifiedName, HandleMessage);
            HostBankOutcomeService();
        }

        public void HandleMessage(Message pMsg)
        {
            if (pMsg.GetType() == typeof(SubmitOrderCommand))
            {
                SubmitOrderCommand lCmd = pMsg as SubmitOrderCommand;
                Order lOrder = lCmd.Order;
                TransferServiceClient lClient = new TransferServiceClient();
                lClient.Transfer((Decimal)lOrder.Total, lOrder.Customer.BankAccountNumber, GetStoreAcctNumber(), ServiceLocator.Current.GetInstance<IOrderProvider>().lastOrderIdProvider() + "," + lOrder.Customer.Email);
            }
        }

        private int GetStoreAcctNumber()
        {
            return 123;
        }

        private void HostBankOutcomeService()
        {
            ServiceHost lHost = new ServiceHost(typeof(BankAdapter));
            lHost.AddServiceEndpoint(typeof(IOperationOutcomeService), new NetMsmqBinding(NetMsmqSecurityMode.None),
               cBankOutcomeAddress);
            lHost.Open();
        }

        public void NotifyOperationOutcome(Bank.Business.Entities.OperationOutcome pOutcome)
        {
            String message,email, bankResponseMessage;
            int orderId;
            bankResponseMessage = pOutcome.Message;

            char[] spilitChars = { ',' };
            string[] splitMessage = bankResponseMessage.Split(spilitChars);

            message = splitMessage[0];
            orderId = Convert.ToInt32(splitMessage[1]);
            email = splitMessage[2];

            if (message.Equals("TransferSuccessful"))
            {
                //transfer failed
                // ServiceLocator.Current.GetInstance<IOrderProvider>().
                Console.WriteLine("sucess");
                
            }
            else
            {
                Console.WriteLine("fail");
                ServiceLocator.Current.GetInstance<IOrderProvider>().failGoBackOrder(orderId);
            }

            ServiceLocator.Current.GetInstance<IOrderProvider>().updateOrderStatus(message,orderId);
            ServiceLocator.Current.GetInstance<IEmailProvider>().SendMessage(email,splitMessage[0]);

        }
    }
}
