﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Entities;

namespace VideoStore.Business.Components.Interfaces
{
    public interface IOrderProvider
    {
        void SubmitOrder(Order pOrder);

        int lastOrderIdProvider();
        void failGoBackOrder(int orderId);
        void updateOrderStatus(string message, int orderId);
    }
}
