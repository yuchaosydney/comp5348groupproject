﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Bank.Business.Components.Interfaces
{
    [ServiceContract]
    public interface ITransferProvider
    {
        [OperationContract(IsOneWay = true)]
        void Transfer(decimal pAmount, int pFromAcctNumber, int pToAcctNumber, String pDescription);
    }
}
