﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Business.Entities;
using System.Transactions;
using VideoStore.Common;
using Microsoft.Practices.ServiceLocation;
using MessageBus.Interfaces;

namespace VideoStore.Business.Components
{
    public class OrderProvider : IOrderProvider
    {
        public void failGoBackOrder(int orderId)
        { 
            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                int orderQuantity = 0;
                int mediaQuantity = 0;

                Order orderModel = lContainer.Orders.Include("OrderItems").Where(x => x.Id == orderId).FirstOrDefault();

                List<OrderItem> orderItemList = orderModel.OrderItems.ToList();

                foreach (OrderItem orderItem in orderItemList)
                {
                    orderQuantity = orderItem.Quantity;
                    //Stock stockModel = lContainer.Stocks.Where(x => x.MediaId == orderItem.MediaId).FirstOrDefault();
                    OrderItem individualOerderItem = lContainer.OrderItems.Include("Media").Where(x => x.Id == orderItem.Id).FirstOrDefault();
                    Media mediaModel = lContainer.Medias.Include("Stocks").Where(x => x.Id == individualOerderItem.Media.Id).FirstOrDefault();
                    mediaModel.Stocks.Quantity = mediaModel.Stocks.Quantity + orderQuantity;
                }
                lContainer.SaveChanges();
                lScope.Complete();
            }
        }
        public void updateOrderStatus(string message, int orderId)
        { 
            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                Order orderModel = lContainer.Orders.Where(x => x.Id == orderId).FirstOrDefault();
                if (message.Equals("TransferSuccessful"))
                {
                    orderModel.Status = 1;
                }
                else
                {
                    orderModel.Status = 2;
                }
                lContainer.SaveChanges();
                lScope.Complete();
            }
        }

        public int lastOrderIdProvider() {
            int lastOrderId;
            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                Order orderModel = (from theOrder in lContainer.Orders
                                    select theOrder).ToList().LastOrDefault();
                if (orderModel == null)
                {
                    lastOrderId = 1;
                }
                else
                {
                    lastOrderId = orderModel.Id + 1;
                }
                lContainer.SaveChanges();
                lScope.Complete();
            }
            return lastOrderId;
        }
        private IEmailProvider EmailProvider
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IEmailProvider>();
            }
        }

        public void SubmitOrder(Entities.Order pOrder)
        {
            String lMessage = String.Format("Your order has been submitted successfully");
            
            try
            {
               
                ServiceLocator.Current.GetInstance<IPublisherService>().Publish(
                    CommandFactory.Instance.GetSubmitOrderCommand(pOrder)
                );
               
            }
            catch (Exception lException)
            {
                lMessage = lException.Message;
                //throw;
            }
            finally
            {
               
                EmailProvider.SendMessage(pOrder.Customer.Email, lMessage);
            }

        }
    }
}
