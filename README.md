Start the applications in the solution by the following order:

* Bank
* MediaRevCo
* VideoStore

Then 4 message queues in the computer in the computer management are created:

* BankQueueTransferTransacted
* BankTransferResponseQueueTransaction
* PublisherReviewMessageQueueTransacted
* PublisherSendReviewMessageQueueTransacted

Type 'localhost:(port number)' in a browser to visit the video store and login with customer account(username:Customer and password:Customer).

###Part A###
Go to 'Browse Media' page to get any media you want to purchase and then go to 'check out' page to check out all your goods in cart. Once the checkout link is clicked, VideoStore will count down each media's stock number according to how many the customer ordered and send the customer information to 'BankQueueTransferTransacted' queue which will be catched by bank instance if it is running.Then the checking result message will be put in the 'BankTransferResponseQueueTransaction' queue by bank after the back checking process.
Based on the bank response VideoStore will be able to know if the customer has enough balance in his or her bank account or not. If the customer has enough balance, VideoStore will inform the customer process is success via email. However, if the customer does not have enough balance, the customer will receive an email informing them they do not have enough balance and purchase failed. In VideoStore side, the stock number will roll back to the original number.
###Part B###
Once the VideoStore starts, it will propogate media items to the 'PublisherSendReviewMessageQueueTransacted' queue (currently 32 items) which will be picked up by MediaRevCo instance once it starts. All medias' msmq addresses will be save in Registry.txt located in '\MediaRevCo.Process\bin\Debug' path.

###Part C###
Submit any media item review you want to review. The MeidaRecCo instance will send review message based on the media items information to 'PublisherReviewMessageQueueTransacted' queue which will be picked up by VideoStore if it is running and shown on the VideoStore ListReviews page.